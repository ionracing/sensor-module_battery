#include "stdint.h"
#include "stdbool.h"
#include "globals.h"
#include "CAN_DEFINES.h"

struct sensor sensors[8];

/* Function prototypes */
void BSP_sensorMapping_Reset(void);
    
void BSP_sensors_init()
{
    #ifdef SM_PEDALS
    sensors[0].channel = 0;
    sensors[0].controlState = true;
    sensors[0].sensorID = 1;
    #elif defined(SM_BATTERY)
    
    #elif defined(SM_BLACKBOX)
    
    #else
        BSP_sensorMapping_Reset();
    #endif
    
}

void BSP_sensorMapping_Reset(void)
{
    uint8_t length = LENGTH_OF_ARRAY(sensors); 
    int n = 0;
    for(n = 0; n<length; n++)
    {
        sensors[n].channel = n;
        sensors[n].controlState = false;
        sensors[n].sensorID = 0;
    }
}

