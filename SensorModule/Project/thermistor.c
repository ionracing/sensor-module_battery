/*
Functions for linearising and scaling thermistor temperature sensors.
*/
#include "stdint.h"
/* Function prototypes ------------------------------------------------------------ */
static uint16_t scale_sensor(uint16_t min_temp, uint16_t max_temp, float temperature);
/* Constants -----------------------------------------------------------------------*/
#define KA_SENSOR 1
#define TEXENSE 2
/* Macros --------------------------------------------------------------------------*/
#define linearize_KA(ADC_CODE)
#define linearize_Texense(ADC_CODE) 

/* Functions -----------------------------------------------------------------------*/
uint16_t thermistor_convertRawData(uint16_t rawData, uint8_t sensorType)
{
    if(sensorType == KA_SENSOR)
    {
        return 1;
    }
    else if(sensorType == TEXENSE)
    {
       return 1; 
    }
    else
    {        
        return 0;
    }
}

/**
*@brief Scales a temperature value to a unsigned 16 bit value corresponding
*       to a 0->100% range given by min_temp and max_temp.
*
*@input uint16_t min_temp: The minimum temperature corresponding to 0%.
*       Must be a value that equals the minimum temperature in celsius 
*       degrees. i.e 0 for a minimum of 0 degrees.
*
*@input uint16_t max_temp: The maximum temperature corresponding to 100%.
*       Must be value that equals the maximum temperature in celsius degrees.
*       i.e 50 for a maximum temperature of 50 degrees celsius.
*
*@input float temperature: The variable to be scaled/converted. Given in
*       celsius degrees.
*
*@output uint16_t scaledTemp. The scaled temperature. 
*       1           = min_temp. 
*       (2^16)-2    = max_temp. 
*       0           = undertemperature.
*       (2^16)-1    = overtemperature.
*/
static uint16_t scale_sensor(uint16_t min_temp, uint16_t max_temp, float temperature)
{
    return 1;
}
