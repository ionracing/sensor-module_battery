/*
* LEDs for debugging purposes. See the h-file for mapping.
*/

/* Includes --------------------------------------------------------*/
#include "BSP_LED.h"

/* Configuration ---------------------------------------------------*/
void BSP_LED_init()
{
	GPIO_InitTypeDef GPIO_initStruct;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_Pin = LED1 | LED2 | LED3 | LED4;
	
	GPIO_Init(GPIOD, &GPIO_initStruct);
}
