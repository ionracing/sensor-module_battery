#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx.h"
#include "config.h"

/* Prototypes */
void BSP_LED_init(void);

/* Defines */
#ifdef DISCOVERY

#define LED_GPIO GPIOD 
#define LED1 GPIO_Pin_12 
#define LED2 GPIO_Pin_13 
#define LED3 GPIO_Pin_14 
#define LED4 GPIO_Pin_15 

#else

#define LED_GPIO GPIOD 
#define LED1 GPIO_Pin_0 
#define LED2 GPIO_Pin_2 
#define LED3 GPIO_Pin_1 
#define LED4 GPIO_Pin_3 

#endif

/* 
Macros: 
For turning ONN/OFF LEDs.
Example: for turning on LED1 write:
LED_ON(LED1);
*/
#define LED_ON(LED) (GPIOD->BSRRL |= LED)
#define LED_OFF(LED) (GPIOD->BSRRH |= LED)
#define LED_TOGGLE(LED) (GPIOD->ODR ^= LED)

#define LED_ALL_ON (LED_ON(LED1 | LED2 | LED3 | LED4))
#define LED_ALL_OFF (LED_OFF = LED1 | LED2 | LED3 | LED4)
#define LED_ALL_TOGGLE (LED_TOGGLE(LED1 | LED2 | LED3 | LED4))
